This code is for reliefweb-training app.

```bash
$ sudo npm install -g ionic cordova
$ ionic serve
```

Then, to run it, cd into `reliefweb-training` and run:

```bash
$ ionic cordova platform add ios
$ ionic cordova run ios
```

Substitute ios for android if not on a Mac.

