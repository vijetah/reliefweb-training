import { Experience , CareerCategory , Theme , Type , OrganizationType } from './search.interface'

export var EXPERIENCES: Experience[] = [
	{ "id": 258, "name": "0-2 years" },
	{ "id": 259, "name": "3-4 years" },
	{ "id": 260, "name": "5-9 years" },
	{ "id": 261, "name": "10+ years" },
];

export var CAREER_CATEGORIES: CareerCategory[] = [
	{"id": 6863,"name": "Administration/HR"},
	{"id": 6867, "name": "Program/Project Management" },
	{"id": 6864, "name": "Finance/Accounting/Auditing"},
	{"id": 20971,"name": "Information Management"},
	{"id": 20966, "name": "Donor Relations/Fundraising/Grants Management"},
	{"id": 6866, "name": "Information Technology"},
	{"id": 6865, "name": "Media/Communication"},
	{"id": 6868, "name": "Monitoring and Evaluation"}
];

export var THEMES: Theme[] = [
	{ "id": 4587, "name": "Agriculture" },
	{ "id": 4588, "name": "Climate Change and Environment" },
	{ "id": 4589, "name": "Contributions" },
	{ "id": 4590, "name": "Coordination" },
	{ "id": 4591, "name": "Disaster Management" },
	{ "id": 4592, "name": "Education" },
	{ "id": 4593, "name": "Food and Nutrition" },
	{ "id": 4594, "name": "Gender" },
	{ "id": 4595, "name": "Health" },
	{ "id": 4596, "name": "HIV/Aids" },
	{ "id": 4597, "name": "Humanitarian Financing" },
	{ "id": 4598, "name": "Logistics and Telecommunications" },
	{ "id": 12033, "name": "Mine Action" },
	{ "id": 4599, "name": "Peacekeeping and Peacebuilding" },
	{ "id": 4600, "name": "Protection and Human Rights" },
	{ "id": 4601, "name": "Recovery and Reconstruction" },
	{ "id": 4602, "name": "Safety and Security" },
	{ "id": 4603, "name": "Shelter and Non-Food Items" },
	{ "id": 4604, "name": "Water Sanitation Hygiene" }
];

export var TYPES: Type[] = [
	{"id":264, "name":"Consultancy"},
	{"id":265, "name":"Internship"},
	{"id":263, "name":"Job"},
	{"id":266, "name":"Volunteer Opportunity"}
];

export var ORGANIZATION_TYPES: OrganizationType[] = [
	{"id": 270, "name": "Academic and Research Institution" },
	{"id": 271, "name":"Government"},
	{"id": 272, "name":"International Organization"},
	{"id": 273, "name":"Media"},
	{"id": 274, "name":"Non-governmental Organization"},
	{"id": 275, "name":"Other"},
	{"id": 276, "name":"Red Cross/Red Crescent Movement"}
];
