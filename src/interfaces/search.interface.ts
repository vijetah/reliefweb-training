export interface  Experience {
	id: number;
	name: string;
}

export interface CareerCategory {
	id: number;
	name: string;
}

export interface Theme {
	id: number;
	name: string;
}

export interface Type {
	id: number;
	name: string;
}

export interface OrganizationType {
	id: number;
	name: string;
}

export interface Search {
	id?:number;
	title: string;
	location_title: string;
	keyword?: string;
	location?: string;
	experience?: Experience;
	career?: CareerCategory;
	type?: Type;
	theme?: Theme;
	organization?: string;
	organization_type?: OrganizationType;
	mapUrlSmall?: string;
	lastJobId?: any;
	numberNews?:number;
	totalJobs?: number;

}
