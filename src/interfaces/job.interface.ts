import { Experience , CareerCategory , Theme , Type } from './search.interface'

export interface Language {
	id: number;
	name: string;
	code: string;
}

export interface DateLog {
	createDate: string;
	changeDate?: string;
	closingDate?: string;
}

export interface Country {
	name: string;
	iso3: string;
	shortname: string;
}

export interface Job {
	trueDate?: any;
	id?: string;
	title?: string;
	url?: string;
	mapUrlSmall?: string;
	mapUrl?: string;
	language?: Language;
	descriptionHtml?: string;
	howToApplyHtml?: string;
	career?: CareerCategory;
	date?: DateLog;
	city?: string;
	country?: Country;
	experience?: Experience;
	theme?: Theme;
	type?: Type;
	source?: string;
	favorite?: boolean;
	new?: boolean;
}
