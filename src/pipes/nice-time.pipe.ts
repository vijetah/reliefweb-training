import { Injectable , Pipe } from '@angular/core';


@Pipe({
  name: 'niceTime'
})


@Injectable()
export class NiceTimePipe {

  transform(value) {
    let date = new Date(value);
    return  date;
  }

}
