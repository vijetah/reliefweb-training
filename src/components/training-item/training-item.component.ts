import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

//Services
import { SearchService    } from '../../services/search.service';
import { SavedJobsService } from '../../services/saved-jobs.service';
//import { TrainingDetailComponent } from '../training-detail/training-detail.component';
import { TrainingDetailsComponent } from '../../pages/training-details/training-details.component';

@Component({
  selector: 'training-item',
  templateUrl: 'training-item.component.html',
  inputs: ['job', 'jobs', 'index', 'lastJobId', 'fromItem','training','trainings']
})

export class  TrainingItemComponent {

  public job      : any;
  public jobs     : any;
  public lastJobId: any;
  public training : any;

  public favorite : boolean;
  public badgeNew : boolean;
  public index    : number;
  public closedend: boolean;
  public fromItem : boolean;

  constructor (
    private nav   : NavController,
    private events: Events,
    private socialSharing: SocialSharing,
    private _searchService  : SearchService,
    private _savedJobService: SavedJobsService
  ) {
      this.favorite  = false;
      this.badgeNew  = false;
      this.closedend = false;
    }

  ngOnInit() {
    // this.setBadgeNew();
    // this.checkFavorite();
    // this.isClosed(this.job.fields.date.closing);

    // this.events.subscribe('refreshJobs', () => this.checkFavorite());
    // this.events.subscribe('removeNew' + this.job.id, () => this.removeBagdeNew());
  }

  isClosed(date) {
    let endDate = new Date(date).getTime();
    let now = new Date().getTime();
    //Close date if < 7 days before the end of the job offer
    this.closedend = (endDate - now) < 604800000;   //604800000 = 7 days
  }

  //------------------------------------------------------------------------//
  //--------------------------------BADGE NEW-------------------------------//
  //------------------------------------------------------------------------//

  setBadgeNew() {
    let lastId = new Date(this.lastJobId).getTime();
    let createdDate = new Date(this.job.fields.date.created).getTime();
    if (this.lastJobId && createdDate > lastId && this.fromItem) {
      this.badgeNew = true;
    }
  }

  removeBagdeNew() {
    this.badgeNew = false;
  }

  //------------------------------------------------------------------------//
  //--------------------------------SAVE JOB--------------------------------//
  //------------------------------------------------------------------------//

  saveJob() {
    this._savedJobService.addSavedJobDB(this.job)
        .then(() => {
          this.checkFavorite();
          this.events.publish('refreshJobs');
        });
  }

  deleteJob() {
    this._savedJobService.removeSavedJobDB(this.job)
        .then(() => {
          this.checkFavorite();
          this.events.publish('refreshJobs');
        });
  }

  checkFavorite() {      //To display the right star button
    this._savedJobService.getIds()
        .then(
          data => {
            let ids = [];
            for (let i = 0 ; i < data.rows.length ; i++) {
              ids.push(data.rows.item(i).id);
            }
            if (ids.indexOf(+this.job.id) != -1) {
              this.favorite = true;
              this.jobs[this.index].favorite = true;
            }
            if (ids.indexOf(+this.job.id) == -1) {
              this.favorite = false;
              this.jobs[this.index].favorite = false;
            }
          },
          err => console.info("Unable to check favorite : ", err)
        );
  }

  //------------------------------------------------------------------------//
  //-------------------------------SHARE JOB--------------------------------//
  //------------------------------------------------------------------------//

  shareJob() {
    this.socialSharing.share("From ReliefWeb Jobs App : " + this.job.fields.title + this.job.fields.url_alias  , "From ReliefWeb Jobs App : " + this.job.fields.title , null , null);
  }

  //------------------------------------------------------------------------//
  //------------------------------NAVIGATION--------------------------------//
  //------------------------------------------------------------------------//

  goToJobDetails(index, training) {
    this.nav.push(TrainingDetailsComponent, {results: this.training, index: this.index});
  }

}
