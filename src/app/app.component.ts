import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
//Components
import { WalkthroughComponent } from '../pages/walkthrough/walkthrough.component';
//Services
import { StorageService     } from '../services/storage.service';
import { SavedSearchService } from '../services/saved-search.service';
import { SavedJobsService   } from '../services/saved-jobs.service';
import { FeaturedPage } from '../pages/featured/featured';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;

  constructor( private platform    : Platform,
    private statusBar   : StatusBar,
    private splashScreen: SplashScreen,
    private _storageService    : StorageService,
    private _savedSearchService: SavedSearchService,
    private _savedJobsService  : SavedJobsService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.initNav();
      this.initDatabase();
    });
  }
  initDatabase() {
    //this._savedSearchService.createJobsDatabase();
    //this._savedJobsService.createJobsDatabase();
  }

  initNav() {
    this._storageService.getItem("walkthrough").then(data => {
      if (data) {   // If we already opened the app : go to HomeComponent
        this.rootPage =TabsPage;// FeaturedPage;
      }
      else {        // If we didn't : go to WalkthroughComponent
        this.rootPage = TabsPage;//WalkthroughComponent;
        this._storageService.setItem("walkthrough", true);
      }
    });
  }
}
