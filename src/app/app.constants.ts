export const APP_NAME = "rwtraining";

export const CONSTANTS: any = {
  JOBS_URL   : 'https://api.reliefweb.int/v1/jobs'      + "?appname=" + APP_NAME,
  SOURCE_URL : 'https://api.reliefweb.int/v1/sources'   + "?appname=" + APP_NAME,
  COUNTRY_URL: 'https://api.reliefweb.int/v1/countries' + "?appname=" + APP_NAME,
  TRAINING_URL:'https://api.reliefweb.int/v1/training'  + "?appname=" + APP_NAME,
  gitRwUrl: 'https://raw.githubusercontent.com/reliefweb/'
};

export const APP_CONSTANTS: any = {
	current_app_ios: 'https://itunes.apple.com/us/app/reliefweb-jobs/id1123682093?ls=1&mt=8',
	current_app_android: 'https://play.google.com/store/apps/details?id=org.reliefweb.jobs'
};
