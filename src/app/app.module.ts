import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule             } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

import { ExplorePage } from '../pages/explore/explore';
import { SavedPage } from '../pages/saved/saved';
import { FeaturedPage } from '../pages/featured/featured';
import { TabsPage } from '../pages/tabs/tabs';
import { ResultsComponent     } from '../pages/results/results.component';
import { TrainingDetailsComponent  } from '../pages/training-details/training-details.component';
import { ApplyComponent       } from '../pages/apply/apply.component';
import { WalkthroughComponent } from '../pages/walkthrough/walkthrough.component';

import { SearchTrainingComponent      } from '../pages/searchtraining/searchtraining.component';
//Native
import { SplashScreen    } from '@ionic-native/splash-screen';
import { StatusBar       } from '@ionic-native/status-bar';
import { SQLite          } from '@ionic-native/sqlite';
import { SocialSharing   } from '@ionic-native/social-sharing';
import { AppVersion      } from '@ionic-native/app-version';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { Geolocation     } from '@ionic-native/geolocation';
import { NativeGeocoder  } from '@ionic-native/native-geocoder';
//Services
import { StorageService     } from '../services/storage.service';
import { DatabaseService    } from '../services/database.service';
import { SearchService      } from '../services/search.service';
import { SavedSearchService } from '../services/saved-search.service';
import { SavedJobsService   } from '../services/saved-jobs.service';
import { GeolocationService } from '../services/geoloc.service';
import { VersionService     } from '../services/version.service';
//Pipes
import { NiceTimePipe } from '../pipes/nice-time.pipe';
import { CategoriesComponent } from '../pages/categories/categories.component';
import { TraininglistComponent } from '../pages/training-list/training-list.component';
import { TrainingItemComponent } from '../components/training-item/training-item.component';

@NgModule({
  declarations: [
    MyApp,
    ExplorePage,
    SavedPage,
    FeaturedPage,
    TabsPage,
    MyApp,
    ResultsComponent,
    TrainingDetailsComponent,
    ApplyComponent,
    WalkthroughComponent,
    NiceTimePipe,
    SearchTrainingComponent,
    CategoriesComponent,
    TraininglistComponent,
    TrainingItemComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    BrowserModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ExplorePage,
    SavedPage,
    FeaturedPage,
    TabsPage,
    MyApp,
    ResultsComponent,
    TrainingDetailsComponent,
    ApplyComponent,
    WalkthroughComponent,
    SearchTrainingComponent,
    CategoriesComponent,
    TrainingItemComponent,
    TraininglistComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    SocialSharing,
    AppVersion,
    GoogleAnalytics,
    Geolocation,
    NativeGeocoder,
    //Services
    StorageService,
    DatabaseService,
    SearchService,
    SavedSearchService,
    SavedJobsService,
    GeolocationService,
    VersionService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
