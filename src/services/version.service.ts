import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Platform, AlertController } from 'ionic-angular';
//Constants
import { CONSTANTS } from '../app/app.constants'
//Native
import { AppVersion } from '@ionic-native/app-version';

@Injectable()
export class VersionService {

	constructor(
		public platform: Platform,
		public alert   : AlertController,
		public http    : Http,
		public appVersion: AppVersion
	) {}

	getAppVersion() {
		return this.appVersion.getVersionNumber().then(
			data => {return data;   },
			err  => {return "-.-.-";}
		);
	}

	getVersions() {
		//Gettin application version
		this.appVersion.getVersionNumber()
				.then(
					data => {
						console.log("VERSION APP : ", data);

						//Gettin minimum version
						this.http.get(CONSTANTS.gitRwUrl + "app-version-manager/master/jobs.json")
								.subscribe(res => {
									console.log("VERSION MIN : ", res.json()[0].minVersion);

									this.checkVersion(data, res.json()[0].minVersion);
								});
					},
					error => console.info("App Version Plugin : Cordova not available")
				);
	}

	checkVersion(app, min) {
		//Creating an array with the version digits to compare them one by one
		let digitsApp = app.split(".").map(digit => +digit);
		let digitsMin = min.split(".").map(digit => +digit);

		if (digitsApp[0] < digitsMin[0]) {
			this.showAlertVersion();
		}
		else {

			if (digitsApp[1] < digitsMin[1]) {
				this.showAlertVersion();
			}
			else {

				if (digitsApp[2] < digitsMin[2]) {
					this.showAlertVersion();
				}
			}
		}
	}

	showAlertVersion() {
		let alert = this.alert.create({enableBackdropDismiss: false});
		alert.setTitle("Update is required");
		alert.setMessage("You are using an old version. Some functionalities might not work properly. Please update");
		alert.addButton({
			text   : "Go to the store",
			handler: () => {
				this.goToStore();
				return false;   //Not to dismiss the alert by clicking on the button
			}
		});
		alert.present();
	}

	goToStore() {
    if (this.platform.is("ios")) {
      window.open("https://itunes.apple.com/us/app/reliefweb-jobs/id1123682093?ls=1&mt=8", '_system', 'location=yes');
    }
    else if (this.platform.is("android")) {
      window.open("https://play.google.com/store/apps/details?id=org.reliefweb.jobs", '_system', 'location=yes');
    }
	}

}
