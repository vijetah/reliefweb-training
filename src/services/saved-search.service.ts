import { Injectable } from '@angular/core';
//Interfaces
import { Search } from '../interfaces/search.interface';
import { EXPERIENCES , TYPES , CAREER_CATEGORIES , THEMES , ORGANIZATION_TYPES} from '../interfaces/criteria.interface';
//Service
import { DatabaseService } from '../services/database.service';


@Injectable()


export class SavedSearchService {

	constructor(
		private _databaseService: DatabaseService
	) {}

	createJobsDatabase() {
		let query = "CREATE TABLE IF NOT EXISTS saved_search(keyword TEXT, location TEXT, experience_id INTEGER , career_id INTEGER, type_id INTEGER , theme_id INTEGER , organization TEXT, organization_id INTEGER, map_url_small TEXT, last_job_id INTEGER, unique(keyword, location, experience_id, career_id, type_id, theme_id, organization, organization_id))";
		this._databaseService.createDatabase('searches.db', query);   // DO NOT change name, users would lose their jobs
	}

	getSavedSearches() {
		let query = 'SELECT rowid , * FROM saved_search';
		return this._databaseService.executeSql(query , null);
	}

	addSavedSearch(search) {
		let query = 'INSERT INTO saved_search (keyword, location, experience_id, career_id, type_id, theme_id, organization, organization_id, map_url_small, last_job_id) VALUES (?,?,?,?,?,?,?,?,?,?)';

		return this._databaseService.executeSql(query, [search.keyword, search.location.toLowerCase(), search.experience.id ? search.experience.id : -1, search.career.id ? search.career.id : -1, search.type.id ? search.type.id : -1, search.theme.id ? search.theme.id : -1, search.organization.toLowerCase(), search.organization_type.id ? search.organization_type.id : -1, search.mapUrlSmall, search.lastJobId]).then(
			() => console.info("Search added"),
			() => console.info("Search already exists")
		);
	}

	updateSavedSearch(search) {
		let query = 'UPDATE saved_search SET last_job_id = ?, map_url_small = ?, keyword = ? , location = ? , experience_id= ? , career_id= ? , type_id = ? , theme_id = ? , organization = ? , organization_id = ? WHERE rowid =' + search.id;

		return this._databaseService.executeSql(query, [search.lastJobId, search.mapUrlSmall, search.keyword.toLowerCase() , search.location , search.experience.id ? search.experience.id : -1 , search.career.id ? search.career.id : -1 , search.type.id ? search.type.id : -1 , search.theme.id ? search.theme.id : -1 , search.organization.toLowerCase() , search.organization_type.id ? search.organization_type.id : -1]).then(
			() => console.info("Search updated"),
			() => {}
		);
	}

	removeSavedSearch(search) {
		let query = 'DELETE FROM saved_search WHERE rowid = ?';

		return this._databaseService.executeSql(query, [search.id]).then(
			() => console.info("Search deleted"),
			() => {}
		);
	}

	formatSearches(rawSearches) {
		let searches: Search[] = [];

		for (let i = 0 ; i < rawSearches.length ; i++) {
			let search = this.createSearch();

			search.id                = rawSearches.item(i).rowid;
			search.keyword           = rawSearches.item(i).keyword;
			search.location          = rawSearches.item(i).location;
			search.experience        = (rawSearches.item(i).experience_id != -1) ?
																 EXPERIENCES.filter(exp => exp.id === rawSearches.item(i).experience_id)[0] :
																 {id: null, name: ""};
			search.type              = (rawSearches.item(i).type_id != -1) ?
																 TYPES.filter(type => type.id === rawSearches.item(i).type_id)[0] :
																 {id: null, name: ""};
			search.career            = (rawSearches.item(i).career_id != -1) ?
																 CAREER_CATEGORIES.filter(carrer => carrer.id === rawSearches.item(i).career_id)[0] :
																 {id: null, name: ""};
			search.theme             = (rawSearches.item(i).theme_id != -1) ?
																 THEMES.filter(theme => theme.id === rawSearches.item(i).theme_id)[0] :
																 {id: null, name: ""};
			search.organization_type = (rawSearches.item(i).organization_id != -1) ?
																 ORGANIZATION_TYPES.filter(ot => ot.id === rawSearches.item(i).organization_id)[0] :
																 {id: null, name: ""};
			search.location_title    = (search.location != "") ?
																 search.location :
																 null;
			search.mapUrlSmall       = (rawSearches.item(i).map_url_small) ?
																 rawSearches.item(i).map_url_small :
																 "assets/images/wld_small.png";
			search.lastJobId         = rawSearches.item(i).last_job_id;
			search.organization      = rawSearches.item(i).organization;
			search.title             = (search.keyword != "") ? search.keyword     :
																 (search.career.id)     ? search.career.name :
																 (search.type.id)       ? search.type.name   :
																 (search.theme.id)      ? search.theme.name  : "All jobs";
			searches.push(search);
		}
		return searches;
	}

	createSearch() {
		let search: Search;

		search = {
			title            : "",
			location_title   : "",
			keyword          : "",
			location         : "",
			experience       : {id: null, name: ""},
			career           : {id: null, name: ""},
			type             : {id: null, name: ""},
			theme            : {id: null, name: ""},
			organization     : "",
			organization_type: {id: null, name: ""}
		};

		return search;
	}

}
