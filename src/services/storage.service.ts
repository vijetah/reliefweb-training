import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';


@Injectable()
export class StorageService {

	constructor(
		private storage: Storage
	) {}

	getItem(item) {
		return this.storage.get(item).then(data => {
			if (data) {
				console.info("Storage : " + item + " available");
				return data;
			}
			else {
				console.info("Storage : " + item + " not available");
				return undefined;
			}
		});
	}

	setItem(item, value) {
		this.storage.set(item, value).then(
			() => console.info("Storage : " + item + " updated")
		);
	}

	removeItem(item) {
		this.storage.remove(item).then(
			() => console.info("Storage : " + item + " removed")
		);
	}

}
