import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { CONSTANTS, APP_NAME } from '../app/app.constants'
import 'rxjs/add/operator/map';


@Injectable()


export class SearchService {

  public logos: any;

  constructor(
    private http: Http,
  ) {
      this.logos = [];
      this.getLogos();
    }

  //------------------------------------------------------------------------//
  //---------------------------------SEARCH---------------------------------//
  //------------------------------------------------------------------------//

  getJobs(search, offset, filter) {
    let params = new URLSearchParams;

    //Preferences
    params.append('limit', '20');
    params.append('offset', offset);
    params.append('sort', 'date.created:desc');
    if (filter == "closing") {
      params.append('sort', 'date.closing:asc');
    }
    if (search.location != "") {
      params.append('filter[operator]', "AND");
      params.append('filter[conditions][0][operator]', "OR");
      params.append('filter[conditions][0][conditions][0][field]', "country");
      params.append('filter[conditions][0][conditions][0][value][]', search.location);
      params.append('filter[conditions][0][conditions][1][field]', "city");
      params.append('filter[conditions][0][conditions][1][value][]', search.location);
      params.append('filter[conditions][0][conditions][2][field]', "country.iso3");
      params.append('filter[conditions][0][conditions][2][value][]', search.location);
    }
    if (search.keyword != "") {
      params.append('filter[operator]', "AND");
    params.append('filter[conditions][1][operator]', "OR");
    params.append('filter[conditions][1][conditions][0][field]', "title");
    params.append('filter[conditions][1][conditions][0][value][]', search.keyword);
    params.append('filter[conditions][1][conditions][1][field]', "body");
    params.append('filter[conditions][1][conditions][1][value][]', search.keyword);
    }
    if (search.experience.id) {
      params.append('filter[operator]', "AND");
      params.append('filter[conditions][2][field]', "experience.id");
      params.append('filter[conditions][2][value][]', search.experience.id);
    }
    if (search.type.id) {
      params.append('filter[operator]', "AND");
      params.append('filter[conditions][3][field]', "type.id");
      params.append('filter[conditions][3][value][]', search.type.id);
    }
    if (search.career.id) {
      params.append('filter[operator]', "AND");
      params.append('filter[conditions][4][field]', "career_categories.id");
      params.append('filter[conditions][4][value][]', search.career.id);
    }
    if (search.theme.id) {
      params.append('filter[operator]', "AND");
      params.append('filter[conditions][5][field]', "theme.id");
      params.append('filter[conditions][5][value][]', search.theme.id);
    }
    if (search.organization != "") {
      params.append('filter[operator]', "AND");
      params.append('filter[conditions][6][field]', "source");
      params.append('filter[conditions][6][value][]', search.organization);
    }
    if (search.organization_type.id) {
      params.append('filter[operator]', "AND");
      params.append('filter[conditions][7][field]', "source.type.id");
      params.append('filter[conditions][7][value][]', search.organization_type.id);
    }

    //Include fields
    params.append('fields[include][]', "id");
    params.append("fields[include][]", "title");
    params.append("fields[include][]", "date");
    params.append("fields[include][]", "country");
    params.append("fields[include][]", "source");
    params.append("fields[include][]", "city");
    params.append("fields[include][]", "language");
    params.append("fields[include][]", "experience");
    params.append("fields[include][]", "theme");
    params.append("fields[include][]", "type");
    params.append("fields[include][]", "career_categories");
    params.append("fields[include][]", "body-html");
    params.append("fields[include][]", "how_to_apply-html");
    params.append("fields[include][]", "url");
    params.append("fields[include][]", "url_alias");

    return this.http.get(CONSTANTS.JOBS_URL, {search: params})
      .map(res => res.json());
  }

  getTrainingFilterCategories(categoryname : any){
    let params = new URLSearchParams;

    //Preferences
    params.append('limit', '100');
    params.append("facets[0][field]", categoryname);
    params.append("facets[0][limit]", "100");

    return this.http.get(CONSTANTS.TRAINING_URL, {search: params}).map(res => res.json());
  }

  getCategoriesList(){

    //https://reliefweb.int/training?country=23.25
    let params = new URLSearchParams;

    //Preferences
    params.append('country', '23.25');
    params.append("fields[include][]", "country");
    params.append("fields[include][]", "source");
    params.append("fields[include][]", "city");
    

    return this.http.get(CONSTANTS.TRAINING_URL, {search: params}).map(res => res.json());
  }

  getLogos() {
    let params = new URLSearchParams;

    //Preferences
    params.append('limit', '1000');
    params.append('filter[conditions][0][field]', "logo");

    //Include fields
    params.append("fields[include][]", "id")
    params.append("fields[include][]", "logo.url");

    return this.http.get(CONSTANTS.SOURCE_URL, {search: params})
      .subscribe(data => {
        this.logos = data.json().data;
        console.log("LOGOS : ", this.logos);
      });
  }

  //------------------------------------------------------------------------//
  //------------------------------PLACEHOLDERS------------------------------//
  //------------------------------------------------------------------------//

  displaySource(job) {
    return job.fields.logo =  {url: "assets/imgs/orgPlaceholder.png"};
    //   let logo = this.logos.filter(logo => {
    //     return logo.fields.id == job.fields.source[0].id;
    //   });
    //   if (!logo[0]){
    //     job.fields.source[0].logo = {url: "assets/images/orgPlaceholder.png"};
    //   }
    //   else {
    //     job.fields.source[0].logo = {url: logo[0].fields.logo.url};
    //   }
    // return job;
  }

  checkNewJobs(search) {
    let params = new URLSearchParams;

    //Preferences
    params.append('limit', '27');
    params.append('offset', '0');
    params.append('sort', 'date.created:desc');
    if (search.location != "") {
      params.append('filter[operator]', "AND");
      params.append('filter[conditions][0][operator]', "OR");
      params.append('filter[conditions][0][conditions][0][field]', "country");
      params.append('filter[conditions][0][conditions][0][value][]', search.location);
      params.append('filter[conditions][0][conditions][1][field]', "city");
      params.append('filter[conditions][0][conditions][1][value][]', search.location);
    }
    if (search.keyword != "") {
      params.append('filter[operator]', "AND");
    params.append('filter[conditions][1][operator]', "OR");
    params.append('filter[conditions][1][conditions][0][field]', "title");
    params.append('filter[conditions][1][conditions][0][value][]', search.keyword);
    params.append('filter[conditions][1][conditions][1][field]', "body");
    params.append('filter[conditions][1][conditions][1][value][]', search.keyword);
    }
    if (search.experience.id) {
      params.append('filter[operator]', "AND");
      params.append('filter[conditions][2][field]', "experience.id");
      params.append('filter[conditions][2][value][]', search.experience.id);
    }
    if (search.type.id) {
      params.append('filter[operator]', "AND");
      params.append('filter[conditions][3][field]', "type.id");
      params.append('filter[conditions][3][value][]', search.type.id);
    }
    if (search.career.id) {
      params.append('filter[operator]', "AND");
      params.append('filter[conditions][4][field]', "career_categories.id");
      params.append('filter[conditions][4][value][]', search.career.id);
    }
    if (search.theme.id) {
      params.append('filter[operator]', "AND");
      params.append('filter[conditions][5][field]', "theme.id");
      params.append('filter[conditions][5][value][]', search.theme.id);
    }
    if (search.organization != "") {
      params.append('filter[operator]', "AND");
      params.append('filter[conditions][6][field]', "source");
      params.append('filter[conditions][6][value][]', search.organization);
    }
    if (search.organization_type.id) {
      params.append('filter[operator]', "AND");
      params.append('filter[conditions][7][field]', "source.type.id");
      params.append('filter[conditions][7][value][]', search.organization_type.id);
    }

    //Include fields
    params.append("fields[include][]", "date.created");

    return this.http.get(CONSTANTS.JOBS_URL, {search: params});
  }

}
