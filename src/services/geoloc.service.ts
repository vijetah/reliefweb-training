import { Injectable  } from '@angular/core';
//Plugins
import { Geolocation    } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';


@Injectable()


export class GeolocationService {

	constructor(
    private geolocation: Geolocation,
    private geocoder   : NativeGeocoder
  ) {}

  getGeolocation() {
    return this.geolocation.getCurrentPosition();
  }

  getCountryByLatLong(latitude, longitude) {
    return this.geocoder.reverseGeocode(latitude, longitude);
  }


}
