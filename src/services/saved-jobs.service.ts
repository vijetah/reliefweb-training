import { Injectable } from '@angular/core';
//Service
import { StorageService  } from '../services/storage.service';
import { DatabaseService } from '../services/database.service';


@Injectable()
export class SavedJobsService {

	constructor(
		private _storageService  : StorageService,
		private _databaseService: DatabaseService
	) {}

	createJobsDatabase() {
		let query = 'CREATE TABLE IF NOT EXISTS saved_jobs(id INTEGER, job TEXT, unique(id, job))';
		this._databaseService.createDatabase('searches.db', query);   // DO NOT change name, users would lose their jobs
	}

	getSavedJobs() {
		let query = 'SELECT rowid, * FROM saved_jobs';

		return this._databaseService.executeSql(query, null);
	}

	addSavedJobDB(job) {
		let query = 'INSERT INTO saved_jobs (id, job) VALUES (?,?)';

		return this._databaseService.executeSql(query, [job.id, JSON.stringify(job)]).then(
			() => console.info("Job added"),
			() => console.info("Job already exists")
		);
	}

	removeSavedJobDB(job) {
		let query = 'DELETE FROM saved_jobs WHERE id = ?';

		return this._databaseService.executeSql(query, [job.id]).then(
			() => console.info("Job deleted"),
			() => {}
		);
	}

	getIds() {
		let query = 'SELECT id FROM saved_jobs';

		return this._databaseService.executeSql(query , null);
	}

	formatJobs(rawJobs) {
		let jobs = [];

		for (let i = 0 ; i < rawJobs.length ; i++) {
			let job = JSON.parse(rawJobs.item(i).job);
			jobs.push(job);
		}
		return jobs;
	}

	//------------------------------------------------------------------------//
	//-------------------------------MIGRATION--------------------------------//
	//------------------------------------------------------------------------//

	migrationDB() {
		this._storageService.getItem("table_id")
			.then(
				data => {
					console.info("Saved jobs in local storage");
					try {
						JSON.parse(data);
						console.info("Bad form : data stringified twice");
					}
					catch (e) {
						console.info("Good form : data stringified once");
						for (let i = 0 ; i < data.length ; i++) {
							this._storageService.getItem("job-" + data[i])
								.then(job => {
									console.info("Job migrated into db : ", job);
									this.addSavedJobDB(job).then(() => {
										this._storageService.removeItem("job-" + data[i]);
									});
								});
						}
						return;
					}
					for (let i = 0 ; i < JSON.parse(data).length ; i++) {
						this._storageService.getItem("job-" + JSON.parse(data)[i])
							.then(oldJob => {
								let job = JSON.parse(oldJob);
								let niceJob = {
									id    : null,
									fields: {
										title            : null,
										source           : [{shortname: null, logo: {url: null}}],
										country          : [{iso3:null, name: null, shortname: null}],
										city             : [{name: null}],
										date             : {created: null, closing: null},
										url_alias        : null
									}
								};
								niceJob.id                          = job.id                ? job.id                         : null;
								niceJob.fields.title                = job.title             ? job.title                      : null;
								niceJob.fields.source[0].shortname  = job.source            ? job.source                     : null;
								niceJob.fields.country[0].iso3      = job.country           ? job.country.iso3               : null;
								niceJob.fields.country[0].name      = job.country           ? job.country.name               : null;
								niceJob.fields.country[0].shortname = job.country           ? job.country.shortname          : null;
								niceJob.fields.date.created         = job.date              ? new Date(job.date.closingDate) : null;
								niceJob.fields.date.closing         = job.date              ? new Date(job.date.createDate)  : null;
								niceJob.fields.url_alias            = job.url               ? job.url                        : null;
								niceJob.fields.source[0].logo.url   = job.mapUrlSmall       ? job.mapUrlSmall                : null;
								niceJob.fields.city[0].name         = job.city              ? job.city                       : null;
								niceJob.fields["body-html"]         = job.descriptionHtml   ? job.descriptionHtml            : null;
								niceJob.fields["how_to_apply-html"] = job.howToApplyHtml    ? job.howToApplyHtml             : null;
								console.info("Job migrated into db : ", niceJob);
								this.addSavedJobDB(niceJob).then(() => {
									this._storageService.removeItem("job-" + JSON.parse(data)[i]);
								});
							});
					}
					this._storageService.removeItem("table_id");
				},
				err => console.info("No saved jobs in local storage")
			);
	}

}
