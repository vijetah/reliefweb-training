import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';


@Injectable()


export class DatabaseService {

	private db: SQLiteObject;

	constructor(
		private sqlite: SQLite
	) {}

	createDatabase(name, query) {
		this.sqlite.create({name: name, location: 'default'}).then(
			db => {
				this.db = db;
				db.executeSql(query, null).then(
					() => console.info("Database : " + name + " table created"),
					() => console.info("Database : unable to create " + name + " table")
				);
			},
			err => {
				this.db = new SQLiteObject(null);   // "Fake" database for non-cordova platforms
				console.warn("Database : Cordova not available");
			}
		);
	}

	executeSql(query, value) {
		return this.db.executeSql(query, value);
	}

}
