import { Component , ViewChild } from '@angular/core';
import { NavParams, Events, LoadingController , Slides , Platform } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { SocialSharing } from '@ionic-native/social-sharing';
//Services
import { SearchService    } from '../../services/search.service';
import { SavedJobsService } from '../../services/saved-jobs.service';

@Component({
  selector   : 'page-training-details',
  templateUrl: 'training-details.component.html'
})
export class TrainingDetailsComponent {

  @ViewChild('jobSlides') slides : Slides;
  slideOptions : any;

  public results: any;
  public jobsTab: any;

  public index       : number;
  public offset      : number;
  public favorite    : boolean;
  public initialSlide: number;

  constructor(
    private params: NavParams,
    private events: Events,
    private load: LoadingController,
    private platform       : Platform,
    private socialSharing: SocialSharing,
    private googleAnalytics: GoogleAnalytics,
    private _searchService  : SearchService,
    private _savedJobService: SavedJobsService
  ) {

    //get variables of result and index
    this.jobsTab = [];
    this.results = this.params.get("results");
    this.index = this.params.get("index");
    // this.initialSlide = (this.results.length <= 5) ? this.index :
    //                     (this.index <= 1) ? this.index :
    //                     (this.index == this.results.length - 1) ? 5 :
    //                     (this.index == this.results.length - 2) ? 3 : 2;
    // this.offset = this.results.length;
    // this.favorite = this.results[this.index].favorite;
  }

  ngOnInit(){
    console.log("INDEX : " , this.index);
    console.log("INDEX : " , this.results);
    // this.googleAnalytics.trackView('JobDetailsPage');

    // this.createAvailableJobs();
    // console.log("INDEX : " , this.index);
    // this.removeBagdeNew();
  }

  removeBagdeNew() {
    this.events.publish('removeNew' + this.results[this.index].id.toString());
  }

  //------------------------------------------------------------------------//
  //--------------------------------SLIDES----------------------------------//
  //------------------------------------------------------------------------//

  //return an array of 5 jobs with the index in the middle
  createAvailableJobs(){
    //Doing it only if there is more than 5 results
    if (this.results.length > 5) {

      //Don't build the array the same way if we click on the two first or the two last jobs
      if (this.index <= 1) {
        for (let i = 0 ; i < 5 ; i++) {
          this.jobsTab.push(this.results[i]);
        }
      }
      else if (this.index > this.results.length - 3) {
        for (let i = 0 ; i < 5 ; i++) {
          this.jobsTab.unshift(this.results[this.results.length - i - 1]);
        }
      }
      else {
        for (let i = 0 ; i < 5 ; i++) {
          this.jobsTab.push(this.results[this.index-2+i]);
        }
      }

    }
    //If there is 5 results or less, just putting them in the array
    else {
      this.jobsTab = this.results;
    }
  }

  slideAction(){
    //To get the right index : the one after the changing
    setTimeout(() => {

      //Not to take the first sliding into account and to make the difference between slide index and job index
      if ((this.slides.getPreviousIndex() != 0 && this.results.length > 5) || this.index == 0) {
        //SLIDE PREVIOUS
        if (this.slides.getPreviousIndex() > this.slides.getActiveIndex()) {

          //When you are on the left
          if (this.index <=2) {
            this.index = this.index - 1;
            console.log("NEX INDEX : " , this.index);
          }
          //When you are on the right
          else if (this.index >= this.results.length - 2) {
            this.index = this.index - 1;
            console.log("NEX INDEX : " , this.index);
          }
          //Normal case
          else {
            this.slidePrevious();
          }

        }
        //SLIDE NEXT
        else {

          //When you are on the left
          if (this.index <= 1) {
            this.index = this.index + 1;
            console.log("NEX INDEX : " , this.index);
          }
          //When you are on the right
          else if (this.index >= this.results.length - 3) {
            this.index = this.index + 1;
            console.log("NEX INDEX : " , this.index);
          }
          //Normal case
          else {
            this.slideNext();
          }
          //Loading the next batch of jobs
          if (this.index == this.results.length - 5) {
            console.log("LOAD MORE JOBS");
            this.events.publish("load_more_jobs");
          }

        }
        //Refreshing the star
        this.favorite = this.results[this.index].favorite;
      }
      else {
        //Refreshing the star
        this.favorite = this.results[this.index].favorite;
      }

      this.removeBagdeNew();
    } , 200);

  }

  //DON'T CHANGE THE ORDER OF THE LINES, IT MATTERS
  slidePrevious(){
    //Removing the last element of the array
    this.jobsTab.splice(4 , 1);
    //Setting the current slide in the middle
    this.slides.slideNext(0 , false);
    //Changinf the index
    this.index = this.index - 1;
    //Inserting a new element in the array at the end
    this.jobsTab.unshift(this.results[this.index - 2]);
    console.log("NEX INDEX : " , this.index);
  }

  //DON'T CHANGE THE ORDER OF THE LINES, IT MATTERS
  slideNext(){
    //Removing the first element of the array
    this.jobsTab.splice(0 , 1);
    //Setting the current slide in the middle
    this.slides.slidePrev(0 , false);
    //Changinf the index
    this.index = this.index + 1;
    //Inserting a new element in the array at the end
    this.jobsTab.push(this.results[this.index + 2]);
    console.log("NEX INDEX : " , this.index);
  }

  //------------------------------------------------------------------------//
  //--------------------------------SAVE JOB--------------------------------//
  //------------------------------------------------------------------------//

  saveJob() {
    this._savedJobService.addSavedJobDB(this.results[this.index])
        .then(() => {
          this.results[this.index].favorite = true;
          this.favorite = this.results[this.index].favorite;
          this.events.publish('refreshJobs');
        });
  }

  deleteJob() {
    this._savedJobService.removeSavedJobDB(this.results[this.index])
        .then(() => {
          this.results[this.index].favorite = false;
          this.favorite = this.results[this.index].favorite;
          this.events.publish('refreshJobs');
        });
  }

  //------------------------------------------------------------------------//
  //-------------------------------SHARE JOB--------------------------------//
  //------------------------------------------------------------------------//

  shareJob() {
    let job = this.results[this.index];
    this.socialSharing.share("From ReliefWeb Jobs App : " + job.fields.title + job.fields.url_alias  , "From ReliefWeb Jobs App : " + job.fields.title , null , null);
  }

}
