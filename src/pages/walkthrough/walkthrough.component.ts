import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

//PAGES
import { FeaturedPage } from '../featured/featured';

@Component({
  selector: 'page-walkthrough',
  templateUrl: 'walkthrough.component.html'
})

export class WalkthroughComponent {

	constructor(
		private nav : NavController
	) {}

  //------------------------------------------------------------------------//
  //------------------------------NAVIGATION--------------------------------//
  //------------------------------------------------------------------------//

  skip() {
    this.nav.setRoot(FeaturedPage);
  }

}
