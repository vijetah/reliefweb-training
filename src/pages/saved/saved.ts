
import { Component } from '@angular/core';
import { NavController, LoadingController , Events , Platform } from 'ionic-angular';
//Native
import { GoogleAnalytics } from '@ionic-native/google-analytics';
//Interfaces
import { Search } from '../../interfaces/search.interface';
//Pages
import { SearchTrainingComponent  } from '../searchtraining/searchtraining.component';
import { ResultsComponent } from '../results/results.component';
//Services
import { SavedSearchService } from '../../services/saved-search.service';
import { SavedJobsService   } from '../../services/saved-jobs.service';
import { SearchService      } from '../../services/search.service';

@Component({
  selector: 'page-saved',
  templateUrl: 'saved.html'
})
export class SavedPage {


  public savedSearches: any;      // Searches saved in database
  public savedJobs    : any;      // Jobs saved in database
  public navbar       : string;   // Choose between saved searches and saved jobs
  public search       : Search;

  constructor (
    private nav     : NavController,
    private load    : LoadingController,
    private event   : Events,
    private platform: Platform,
    private googleAnalytics: GoogleAnalytics,
    private _savedSearchService: SavedSearchService,
    private _savedJobService   : SavedJobsService,
    private _searchService     : SearchService
  ) {
      this.savedSearches = [];
      this.savedJobs     = [];
      this.navbar        = "searches";
      this.search        = this._savedSearchService.createSearch();
    }

    ionViewDidLoad() {
      // this.googleAnalytics.trackView('HomePage');
  }
  
 

  //------------------------------------------------------------------------//
  //------------------------------NAVIGATION--------------------------------//
  //------------------------------------------------------------------------//

  setNavbar(navbar) {
    this.navbar = navbar;
  }

  goToSearch() {
    this.nav.push(SearchTrainingComponent);
  }

}
