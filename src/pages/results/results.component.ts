import { Component } from '@angular/core';
import { NavController , NavParams, Events, LoadingController } from 'ionic-angular';
//Interfaces
import { Search } from '../../interfaces/search.interface';
//Natives
import { GoogleAnalytics } from '@ionic-native/google-analytics';
//Pages
import { FeaturedPage   } from '../featured/featured'
import { SearchTrainingComponent } from '../searchtraining/searchtraining.component';
//Services
import { SearchService } from '../../services/search.service';

@Component({
  selector: 'page-results',
  templateUrl: 'results.component.html'
})

export class ResultsComponent {

  public results    : any;
  public search     : Search;
  public totalCount : number;

  public offset  : number;
  public filter  : string;
  public fromItem: string;

  constructor(
    private nav  : NavController,
    private param: NavParams,
    private event: Events,
    private load : LoadingController,
    private googleAnalytics: GoogleAnalytics,
    private _searchService: SearchService
  ) {
      this.results     = this.param.get("jobs");
      this.search      = this.param.get("search");
      this.totalCount  = this.param.get("totalCount");

      this.offset   = 0;
      this.filter   = "created";
      this.fromItem = this.param.get("fromItem");
    }

  ngOnInit() {
    // this.googleAnalytics.trackView('ResultsPage');

    this.displaySources(this.results);
    this.event.subscribe("load_more_jobs",
      () => this.loadMoreJobs()
    );

  }

  displaySources(jobs) {
    for (let i = 0 ; i < jobs.length ; i++) {
      this._searchService.displaySource(jobs[i]);
    }
  }

  //------------------------------------------------------------------------//
  //--------------------------------SEARCH----------------------------------//
  //------------------------------------------------------------------------//

  refreshJobs(refresher) {
    setTimeout(() => {
      this._searchService.getJobs(this.search, 0, this.filter)
          .subscribe(
            data => {
              console.log("JOBS : ", data.data);
              this.results    = data.data;
              this.totalCount = data.totalCount;
              this.displaySources(this.results);
              refresher.complete();
            },
            error => {
              refresher.complete();
            }
          );
    }, 500);
  }

  sortBy(filter){
    if (this.filter != filter) {
      this.filter = filter;
      this.offset   = 0;

      let loader = this.load.create({spinner: 'crescent', content: 'Sorting ...'});
      loader.present();

      this._searchService.getJobs(this.search, 0, filter)
          .subscribe(
            data => {
              console.log("JOBS : ", data.data);
              loader.dismiss();
              this.results    = data.data;
              this.totalCount = data.totalCount;
              this.displaySources(this.results);
            },
            error => {
              loader.dismiss();
            }
          );
    }
  }

  loadMoreJobs(scroller?) {
    this.offset = this.offset + 20;
    this._searchService.getJobs(this.search, this.offset, this.filter)
        .subscribe(
          data => {
            console.log("JOBS : ", data.data);
            this.totalCount = data.totalCount;
            for (let i = 0 ; i < data.data.length ; i++) {
              this.results.push(data.data[i]);
            }
            this.displaySources(this.results);
            if (scroller) {
              scroller.complete();
            }
          },
          error => {
            if (scroller) {
              scroller.complete();
            }
          }
        );
  }

  //------------------------------------------------------------------------//
  //------------------------------NAVIGATION--------------------------------//
  //------------------------------------------------------------------------//

  goToHome(){
      this.nav.push(FeaturedPage);
  }

  goToSearch(){
    this.nav.push(SearchTrainingComponent);
  }

}
