import { Component } from '@angular/core';

import { ExplorePage } from '../explore/explore';
import { SavedPage } from '../saved/saved';
import { FeaturedPage } from '../featured/featured';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = FeaturedPage;
  tab2Root = ExplorePage;
  tab3Root = SavedPage;

  constructor() {

  }
}
