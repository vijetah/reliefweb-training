import { Component } from '@angular/core';
import { NavController , NavParams, Events, LoadingController } from 'ionic-angular';
//Interfaces
import { Search } from '../../interfaces/search.interface';
//Natives
import { GoogleAnalytics } from '@ionic-native/google-analytics';
//Pages
import { FeaturedPage   } from '../featured/featured'
import { SearchTrainingComponent } from '../searchtraining/searchtraining.component';
//Services
import { SearchService } from '../../services/search.service';
import { TraininglistComponent } from '../training-list/training-list.component';

@Component({
  selector: 'page-categories',
  templateUrl: 'categories.component.html'
})

export class CategoriesComponent {

  public category   : any;
  public results    : any;
  public search     : Search;
  public totalCount : number;

  constructor(
    private nav  : NavController,
    private param: NavParams,
    private event: Events,
    private load : LoadingController,
    private googleAnalytics: GoogleAnalytics,
    private _searchService: SearchService
  ) {
      this.category    = this.param.get("category");
      this.results     = '';
      this.search      = this.param.get("search");
      this.totalCount  = 0;
    }

  ngOnInit() {    
      console.log('---------'+this.category);
    this.getTrainingFilter(this.category);
  }

  //------------------------------------------------------------------------//
  //--------------------------------SEARCH----------------------------------//
  //------------------------------------------------------------------------//

  getTrainingFilter(category: any){
    this._searchService.getTrainingFilterCategories(category)
    .subscribe(
      data => {
        this.results    = data.embedded.facets[this.category].data;
        this.totalCount = data.totalCount;
      },
      error => {
        console.log('errors');
      }
    );
  }

  displayTraining(index,trainingcategory){
      console.log('clicked index '+index+'----');
      this.nav.push(TraininglistComponent);
  }

  //------------------------------------------------------------------------//
  //------------------------------NAVIGATION--------------------------------//
  //------------------------------------------------------------------------//

  goToHome(){
      this.nav.push(FeaturedPage);
  }

  goToSearch(){
    this.nav.push(SearchTrainingComponent);
  }

}
