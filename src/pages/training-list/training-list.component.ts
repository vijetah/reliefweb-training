import { Component } from '@angular/core';
import { NavController , NavParams, Events, LoadingController } from 'ionic-angular';
//Interfaces
import { Search } from '../../interfaces/search.interface';
//Natives
import { GoogleAnalytics } from '@ionic-native/google-analytics';
//Pages
import { FeaturedPage   } from '../featured/featured'
import { SearchTrainingComponent } from '../searchtraining/searchtraining.component';
//Services
import { SearchService } from '../../services/search.service';

@Component({
  selector: 'page-training-list',
  templateUrl: 'training-list.component.html'
})

export class TraininglistComponent {

  public results    : any;
  public search     : Search;
  public totalCount : number;

  public offset  : number;
  public filter  : string;
  public fromItem: string;

  constructor(
    private nav  : NavController,
    private param: NavParams,
    private event: Events,
    private load : LoadingController,
    private googleAnalytics: GoogleAnalytics,
    private _searchService: SearchService
  ) {
      this.results     = this.param.get("jobs");
      this.search      = this.param.get("search");
      this.totalCount  = this.param.get("totalCount");

      this.offset   = 0;
      this.filter   = "created";
      this.fromItem = this.param.get("fromItem");
    }

  ngOnInit() {
    // this.googleAnalytics.trackView('ResultsPage');
    this.getCategoryList();
    }

    getCategoryList(){
      this._searchService.getCategoriesList()
      .subscribe(
        data => {
          this.results    = data.data;
              this.totalCount = data.totalCount;
              this.displaySources(this.results);
        },
        error => {
          console.log('errors');
        }
      );
    }
  

  displaySources(jobs) {
    for (let i = 0 ; i < jobs.length ; i++) {
      this._searchService.displaySource(jobs[i]);
    }
  }

  //------------------------------------------------------------------------//
  //------------------------------NAVIGATION--------------------------------//
  //------------------------------------------------------------------------//

  goToHome(){
      this.nav.push(FeaturedPage);
  }

  goToSearch(){
    this.nav.push(SearchTrainingComponent);
  }

}
