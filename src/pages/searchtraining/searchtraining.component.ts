import { Component } from '@angular/core';

import { ResultsComponent } from '../results/results.component';
//Services
import { SavedSearchService } from '../../services/saved-search.service';
import { SearchService      } from '../../services/search.service';
import { GeolocationService } from '../../services/geoloc.service';

@Component({
  selector   : 'page-training-search',
  templateUrl: 'searchtraining.component.html'
})
export class SearchTrainingComponent {

    public results: any;

    public option: boolean;
    public myLocation: boolean;
    public searchLocation: boolean;

  constructor(
    private _savedSearchService: SavedSearchService,
    private _searchService     : SearchService,
    private _geolocationService: GeolocationService
  ) {
      
    }

  ngOnInit() {
    // this.googleAnalytics.trackView('SearchPage');

  }

  
}
