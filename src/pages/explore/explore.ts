import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
//Pages
import { SearchTrainingComponent  } from '../searchtraining/searchtraining.component';
import { ResultsComponent } from '../results/results.component';
import { CategoriesComponent } from '../categories/categories.component';

@Component({
  selector: 'page-explore',
  templateUrl: 'explore.html'
})
export class ExplorePage {

  constructor(public navCtrl: NavController) {

  }

 public goToSearch = function(state){
    console.log('clicked'+state);
    this.navCtrl.push(CategoriesComponent,{category:state});
  }

  public goToMainSearch = function(){
    this.navCtrl.push(SearchTrainingComponent);
  }  
}
