import { Component } from '@angular/core';
import { NavController, LoadingController , Events , Platform } from 'ionic-angular';
//Native
import { GoogleAnalytics } from '@ionic-native/google-analytics';
//Interfaces
import { Search } from '../../interfaces/search.interface';
//Pages
import { SearchTrainingComponent  } from '../searchtraining/searchtraining.component';
import { ResultsComponent } from '../results/results.component';
//Services
import { SavedSearchService } from '../../services/saved-search.service';
import { SavedJobsService   } from '../../services/saved-jobs.service';
import { SearchService      } from '../../services/search.service';


@Component({
  selector   : 'page-featured',
  templateUrl: './featured.html'
})


export class FeaturedPage {

  public savedSearches: any;      // Searches saved in database
  public savedJobs    : any;      // Jobs saved in database
  public navbar       : string;   // Choose between saved searches and saved jobs
  public search       : Search;

  constructor (
    private nav     : NavController,
    private load    : LoadingController,
    private event   : Events,
    private platform: Platform,
    private googleAnalytics: GoogleAnalytics,
    private _savedSearchService: SavedSearchService,
    private _savedJobService   : SavedJobsService,
    private _searchService     : SearchService
  ) {
      this.savedSearches = [];
      this.savedJobs     = [];
      this.navbar        = "searches";
      this.search        = this._savedSearchService.createSearch();
    }

  ionViewDidLoad() {
    // this.googleAnalytics.trackView('FeaturedPage');

    //Choosing the first page
    this.goToTheFirstPage();
  }

  //------------------------------------------------------------------------//
  //--------------------------------SEARCH----------------------------------//
  //------------------------------------------------------------------------//

  searchJobs() {
    let loader = this.load.create({spinner: 'crescent', content: 'Searching ...'});
    loader.present();

    console.log("SEARCH : ", this.search);
    this._searchService.getJobs(this.search, 0, "created")
        .subscribe(
          data => {
            console.log("JOBS : ", data.data);
            loader.dismiss();
            this.nav.push(ResultsComponent, {jobs: data.data, search: this.search, totalCount: data.totalCount});
            if (data.data[0]) {
              this.search.lastJobId = data.data[0].fields.date.created;
            }
            this._savedSearchService.addSavedSearch(this.search);
          },
          error => {
            loader.dismiss();
          }
        );
  }

  //------------------------------------------------------------------------//
  //------------------------------NAVIGATION--------------------------------//
  //------------------------------------------------------------------------//

  setNavbar(navbar) {
    this.navbar = navbar;
  }

  goToSearch() {
    this.nav.push(SearchTrainingComponent);
  }

  goToTheFirstPage() {
    if (this.platform.is('cordova')) {
      this._savedSearchService.getSavedSearches()
          .then(
            data => {
              if (data.rows.length == 0) {
                this.nav.push(SearchTrainingComponent);
              }
            },
            () => {}
          );
    }
  }

}
