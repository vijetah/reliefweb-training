import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';


@Component({
  selector: 'page-apply',
  templateUrl: 'apply.component.html'
})


export class ApplyComponent {

	job : any;

	constructor(
		private params : NavParams
	) {
			this.job = this.params.get("job");
		}

}
